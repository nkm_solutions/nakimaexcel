<?php

use PHPUnit\Framework\TestCase;

use Nakima\Excel\Excel;

class MainTest extends TestCase
{
    public function testAdd() {
    	$excel = new Excel("/home/xgc1986/demo.xls", "Nakima\\Excel\\Demo", "Demo");
    	$excel->createOutput("_output_3", "B7", "result");

    	$excel->createInput("_input_", "B1", "username");
    	$excel->createInput("_input_", "B2", "name");
    	$excel->createInput("_input_", "B3", "surname");
    	$excel->createInput("_input_", "B4", "now");//(new DateTime())->format("d/m/Y")
    	$excel->createInput("_input_", "B5", "birthday");
    	$excel->createInput("_input_", "B6", "gender");
    	$excel->createInput("_input_", "B7", "day");
    	$excel->createInput("_input_", "B8", "disponibility");
    	$excel->createInput("_input_", "B9", "status");
    	$excel->createInput("_input_", "B10", "weight");
    	$excel->createInput("_input_", "B11", "height");
    	$excel->createInput("_input_", "B12", "test6");
    	$excel->createInput("_input_", "B13", "fcTest6");
    	$excel->createInput("_input_", "B14", "fcRTest6");

    	$this->build("/home/xgc1986/Projects/Libs/NakimaExcel/src/Demo");
    }

}