Como crear un proyecto
======================

* Clonas este repositorio `git clone git@bitbucket.org:nkm_solutions/basephpproject.git my-proyect`
* `cd my-proyect`
* `rm -RF .git`
* `composer update`
* Opcionalmente `composer autoload dump`
* Abres el archivo composer.json y cambias 'Base' por el namespace base de tu proyecto
* FIN

Revisar todos los pasos sobretodo