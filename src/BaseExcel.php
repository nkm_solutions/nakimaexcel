<?php
declare(strict_types=1);
namespace Nakima\Excel;

/**
 * Class BaseExcel
 * @package Nakima\Excel
 * @method loadInputExcel()
 * @method loadOutput_3Excel()
 */
class BaseExcel
{
    public $sheets;
    public $cells;
    public $parent;

    public function __construct($parent = null)
    {
        $this->parent = $parent;
        $this->cells = [];
    }

    public function __get($field)
    {
        return $this->{$field};
    }

    public function __set($field, $value)
    {
        $this->{$field} = $value;
        return $this;
    }

    public function _call($field)
    {
        return $this->{$field}();
    }

    public function _callGet($field)
    {
        return $this->{'get' . ucfirst($field)}();
    }


    public function loadSheet(string $s): ?BaseExcel
    {
        return null;
    }

    public function getCells()
    {
        if (!$this->cells) {
            $this->cells = [];
        }
        return $this->cells;
    }
}
