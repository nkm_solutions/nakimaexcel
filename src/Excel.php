<?php
declare(strict_types=1);

namespace Nakima\Excel;

/**
 * @author xgonzalez@nakima.es
 */
class Excel
{

    protected $parser;
    protected $php;

    /**
     * Excel constructor.
     * @param string $path donde quieres que se ponga los php compilados
     * @param string $namespace namespace del lugar donde estan los archivos compilados
     * @param string $class nombre de la clase principal del excel
     * @param string $src lugar donde esta el excel a parsear
     */
    public function __construct(string $path, string $namespace, string $class, string $src)
    {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $this->php = new PHP($namespace, $class);
        $this->parser = new Parser($src, $this->php);
    }

    public function build($src)
    {
        //echo $this->parser->build($src);
        return $this->parser->build($src);
    }

    /**
     * createInput("_input_", "B6", "gender")
     * @param $sheet
     * @param $cell
     * @param $name
     * @return $this
     */
    public function createInput($sheet, $cell, $name)
    {
        $this->php->getSheet($sheet)->addInput($cell, $name);
        return $this;
    }

    public function createOutput($sheet, $cell, $alias = false)
    {
        $this->parser->convert("$sheet", $cell);
        if ($alias) {
            $this->php->getSheet($sheet)->addOutput($cell, $alias);
        }
        return $this;
    }
}
