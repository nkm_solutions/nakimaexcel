<?php
declare(strict_types=1);
namespace Nakima\Excel;

use Nakima\Utils\Time\DateTime;

/**
 * @author xgonzalez@nakima.es
 */
class Functions
{

    public static function _IF($a, $b, $c)
    {
        if ($a) {
            return $b;
        }

        return $c;
    }

    public static function _ISBLANK($a)
    {
        return $a === null;
    }

    public static function _SUM($a)
    {
        $ret = 0;

        foreach (func_get_args() as $value) {
            if (is_array($value)) {
                foreach ($value as $val) {
                    $ret += floatval($val);
                }
            } else {
                $ret += floatval($value);
            }
        }

        return $ret;
    }

    public static function _POWER($a, $b)
    {
        return pow($a, $b);
    }

    public static function _NOW()
    {
        return new \DateTime();
    }

    public static function _SQRT($a)
    {
        return sqrt($a);
    }

    public static function _ROUND($a, $b)
    {
        return round($a, $b);
    }

    public static function _AVERAGE($a)
    {
        $sum = 0;
        foreach ($a as $value) {
            $sum += $value;
        }

        return $sum / count($a);
    }

    public static function _YEAR($a)
    {
        if ($a instanceof \DateTime) {
            return intval($a->format("Y"));
        }
        if (is_string($a)) {
            if (is_numeric($a)) {
                return intval(DateTime::fromFormat("U", intval($a))->format("Y"));
            } else {
                return intval(DateTime::fromFormat("d-m-Y", $a)->format("Y"));
            }
        }

        return intval(DateTime::fromExcel($a)->format("Y"));
    }

    public static function _SLOPE(array $x, array $y)
    {
        return self::linearRegression($y, $x)['m'];
    }

    public static function _INTERCEPT(array $x, array $y)
    {
        return self::linearRegression($y, $x)['b'];
    }

    public static function convertSerialDate($a)
    {
        $timestamp = ($a - 25569) * 86400;

        return date("d/m/Y H:i:s", $timestamp);
    }

    public static function linearRegression(array $x, array $y): array
    {

        for ($i = 0; $i < count($x); $i++) {
            $x[$i] = floatval($x[$i]);
        }

        for ($i = 0; $i < count($y); $i++) {
            $y[$i] = floatval($y[$i]);
        }

        // calculate number points
        $n = count($x);

        // ensure both arrays of points are the same size
        if ($n != count($y)) {

            trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
        }

        // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($y);

        $xx_sum = 0;
        $xy_sum = 0;

        for ($i = 0; $i < $n; $i++) {

            $xy_sum += ($x[$i] * $y[$i]);
            $xx_sum += ($x[$i] * $x[$i]);
        }

        // calculate slope
        $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));

        // calculate intercept
        $b = ($y_sum - ($m * $x_sum)) / $n;

        // return result
        return ["m" => $m, "b" => $b];
    }
}
