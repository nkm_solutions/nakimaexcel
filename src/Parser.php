<?php

namespace Nakima\Excel;

/**
 * @author xgonzalez@nakima.es
 */

class Parser
{

    protected static $COLS
        = ["",
           "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
           "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
           "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
           "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ",
        ];

    private $MAP = [];

    const INV_REG_METHOD      = "/[a-zA-Z]+[^\)\(]+/";
    const EMPTY_METHOD        = "/[a-zA-Z]+()/";
    const HAS_PARENTHESIS     = "/[\()]/";
    const INV_REG_PARENTHESIS = "/[^\)\(]+/";
    const REG_OUTER_VAR       = "/[$]*[']*([a-zA-Z0-9_ ]*)[']*\![$]*([A-Z]+)[$]*([0-9]+)/i";
    const REG_OUTER_RANGE     = "/[$]*[']*([a-zA-Z0-9_ ]*)[']*\![$]*([A-Z]+)[$]*([0-9]+):[$]*([A-Z]+)[$]*([0-9]+)/i";
    const REG_INNER_VAR       = "/[$]*([A-Z]+)[$]*([0-9]+)/i";
    const REG_INNER_RANGE     = "/[$]*([A-Z]+)[$]*([0-9]+):[$]*([A-Z]+)[$]*([0-9]+)/i";

    private $out          = '';
    private $includeInput = false;
    private $sheet;
    private $cell;

    private $debug = false;

    private $excel;

    private $pages = [];

    public function ePrint($b)
    {
        $this->debug = ($b);
    }

    public function __construct($path, $php)
    {
        $this->excel = \PHPExcel_IOFactory::load($path);
        $this->php   = $php;
    }

    public function convert($sheet, $cell)
    {

        if (!isset($this->MAP[$sheet])) {
            $this->MAP[$sheet] = [];
        }

        if (!isset($this->MAP[$sheet][$cell])) {
            $this->MAP[$sheet][$cell] = 1;
        } else {
            $this->printVar("cell", $this->MAP[$sheet][$cell]);

            return "";
        }

        /*if (!isset($this->pages[$sheet])) {
            $class = preg_replace("/ /i", "_", $sheet);
            $this->pages[$sheet] = 1;
            $this->out .= '$output["' . $class . '"] = [];' . "\n";
        }*/
        $aSheet = $this->php->getSheet($sheet);

        $this->loadWorkspace($sheet);

        $query = trim($this->cleanQuery($this->excel->getActiveSheet()->getCell($cell)->getValue()));

        $this->printVar("query", $query);
        $this->printVar("sheet", $sheet);
        $this->printVar("cell", $cell);

        if ($query === "") {
            $ret = "NULL";
        } else if ($query[0] === "=") {// we have an output
            $this->printLine();
            $this->printVar("Original Query", $cell . $query);
            $this->printLine();
            $ret = $this->_parser($sheet, $cell, substr($query, 1));
        } else {
            if ($query === "") {
                $this->printVar("Original Query", $cell . "=NULL");
                $ret = "NULL";
            } else {
                $this->printVar("Original Query", $cell . $query);
                $ret = $query;
            }
        }

        $class = preg_replace("/ /i", "_", $sheet);

        //$this->out .= '$output["' . $class . '"]["' . $cell . '"] = ' . $ret . ";\n";
        $cell   = strtoupper($cell);
        $method =  "    private \$$cell;";
        $method .= "    public function get$cell() {\n";
        $method .= "        //return $ret;\n";
        $method .= "        if (\$this->$cell === null) {\n";
        $method .= "            \$this->$cell = $ret;\n";
        $method .= "        }\n";
        $method .= "        return \$this->$cell;\n";
        $method .= "    }\n\n";
        $aSheet->addMethod($method, $cell);

    }

    public function print()
    {
        if ($this->debug) {
            $this->printLine();
            $this->printVar("RESULT", "\n" . $this->out);
            $this->printLine();
        } else {
            echo $this->out . "\n";
        }
    }

    public function build($src)
    {
        return $this->php->generate($src);
    }

    private function loadWorkspace($name)
    {
        $this->sheet = $this->excel->getSheetByName($name);
        $this->excel->setActiveSheetIndexByName($name);
    }

    /**
     * LEVEL 0 retorna la conversion de una query
     */
    private function _parser($sheet, $cell, $query)
    {

        $currentSheet = $sheet;

        $this->printVar("Parsing: ", $query);

        // Siempre cada operacion resultará en blqoues de parentesis
        $ret = "";

        $b = $this->divideIntoBlocks($sheet, $cell, $query);

        if ($b) {
            return "($b)";
        }

        // Estamos tratando una sola query
        $b = $this->isAParenthesisGroup($query);

        if ($b) {
            // quitamos los parentesis de los extremos
            $query = substr($query, 1, -1);
            $ret   = $this->_parser($sheet, $cell, $query);

            return "($ret)";
        }

        // Estamos tratando una función
        $method = $this->_parseMethod($query);

        if ($method) {

            $ret .= "Functions::_" . strtoupper($method[0]) . "(";

            $params = $this->_parseParams($sheet, $cell, $method[1]);

            foreach ($params as $key => $value) {
                if ($key) {
                    $ret .= ", ";
                }
                $ret .= $this->_parser($sheet, $cell, $value);
            }

            $ret .= ")";

            return "($ret)";
        }

        // Vamos a tratar accesos a hojas externas
        $b = $this->_parseVar1($query);

        if ($b) {
            // estamos tratando con una lista?
            $sheet = $b[1];
            $class = preg_replace("/ /i", "_", $b[1]);
            if (count($b) <= 4) {
                $cell = strtoupper($b[2] . $b[3]);
                // registrar para poder hacer la parte iterativa
                // XXX if ($this->includeInput || $sheet != "_input_") {
                $this->convert($sheet, $cell);
                // XXX}
                //$ret = '($output["' . $class. '"]["' . $cell . '"])';

                $aSheet = $this->php->getSheet($sheet)->getClass();
                if ($currentSheet == $sheet) {
                    $ret = "\$this->get$cell()";
                } else {
                    $ret = "\$this->loadSheet('$aSheet')->get$cell()";
                }

                return $ret;
            } else {
                // Recorremos en filas o columnas
                if ($b[2] == $b[4]) {
                    $this->printVar("Array", "filas");
                    // recorremos incrementando la fila (numero) :D

                    $ret = "([";

                    for ($i = $b[3]; $i <= $b[5]; $i++) {
                        if (!($i == $b[3])) {
                            $ret .= ",";
                        }
                        $cell = strtoupper($b[2] . $i);
                        // registrar para poder hacer la parte iterativa
                        // XXX if ($this->includeInput || $sheet != "_input_") {
                        $this->convert($sheet, $cell);
                        // XXX }
                        //$ret .= '($output["' . $class. '"]["' . $b[2] . $i . '"])';
                        $aSheet = $this->php->getSheet($sheet)->getClass();
                        if ($currentSheet == $sheet) {
                            $ret .= "\$this->get$cell()";
                        } else {
                            $ret .= "\$this->loadSheet('$aSheet')->get$cell()";
                        }
                    }

                    $ret .= "])";

                    return $ret;
                } else {
                    $this->printVar("Array", "columnas");
                    for ($i = $b[2]; true; $i = $this->incrCol($i)) {
                        if (!($i == $b[2])) {
                            $ret .= ",";
                        }
                        $cell = strtoupper($i . $b[3]);
                        // XXX if ($this->includeInput || $sheet != "_input_") {
                        $this->convert($sheet, $cell);
                        // XXX }
                        //$ret .= '($output["' . $class. '"]["' . $cell . '"])';
                        $aSheet = $this->php->getSheet($sheet)->getClass();
                        if ($currentSheet == $sheet) {
                            $ret .= "\$this->get$cell()";
                        } else {
                            $ret .= "\$this->loadSheet('$aSheet')->get$cell()";
                        }
                        if ($b[4] == $i) {
                            break;
                        }
                    }

                    return $ret;
                    // recorremos incrementando la columna (numero) D:
                }
            }
        }

        $b = $this->_parseVar2($query);
        if ($b) {
            // estamos tratando con una lista?
            $class = preg_replace("/ /i", "_", $sheet);
            if (count($b) <= 4) {
                $cell = $b[1] . $b[2];
                // registrar para poder hacer la parte iterativa
                //XXX if ($this->includeInput || $sheet != "_input_") {
                $this->convert($sheet, $cell);
                //XXX }
                //$ret = '($output["' . $class. '"]["' . $cell . '"])';
                $aSheet = $this->php->getSheet($sheet)->getClass();

                if ($currentSheet == $sheet) {
                    $ret = "\$this->get$cell()";
                } else {
                    $ret = "\$this->loadSheet('$aSheet')->get$cell()";
                }

                return $ret;
            } else {
                // Recorremos en filas o columnas
                if ($b[1] == $b[3]) {
                    $this->printVar("Array", "filas");
                    // recorremos incrementando la fila (numero) :D

                    $ret = "([";

                    for ($i = $b[2]; $i <= $b[4]; $i++) {
                        if (!($i == $b[2])) {
                            $ret .= ",";
                        }
                        $cell = strtoupper($b[1] . $i);
                        // registrar para poder hacer la parte iterativa
                        //XXX if ($this->includeInput || $sheet != "_input_") {
                        $this->convert($sheet, $cell);
                        //XXX }
                        //$class = preg_replace("/ /i", "_", $sheet);
                        //$ret .= '($output["' . $class. '"]["' . $b[1] . $i . '"])';
                        $aSheet = $this->php->getSheet($sheet)->getClass();

                        if ($currentSheet == $sheet) {
                            $ret .= "\$this->get$cell()";
                        } else {
                            $ret .= "\$this->loadSheet('$aSheet')->get$cell()";
                        }
                    }

                    $ret .= "])";

                    return $ret;
                } else {
                    for ($i = $b[1]; true; $i = $this->incrCol($i)) {
                        if (!($i == $b[1])) {
                            $ret .= ",";
                        }
                        $cell = strtoupper($i . $b[2]);
                        // registrar para poder hacer la parte iterativa
                        //XXX if ($this->includeInput || $sheet != "_input_") {
                        $this->convert($sheet, $cell);
                        //XXX }
                        //$class = preg_replace("/ /i", "_", $sheet);
                        //$ret .= '($output["' . $class. '"]["' . $cell . '"])';
                        $aSheet = $this->php->getSheet($sheet)->getClass();
                        if ($currentSheet == $sheet) {
                            $ret .= "\$this->get$cell()";
                        } else {
                            $ret .= "\$this->loadSheet('$aSheet')->get$cell()";
                        }
                        if ($b[3] == $i) {
                            break;
                        }
                    }

                    return $ret;
                    // recorremos incrementando la columna (numero) D:
                }
            }
        }

        $matches = [];
        $b       = preg_match(self::EMPTY_METHOD, $query, $matches);
        if ($b) {
            return "Functions::_$query";
        }

        return "($query)";
    }

    /**
     * LEVEL 1 - Dividimos la query en posibles subqueries, si la cantidad de estas
     * es mayor que 1 entonces repetimos el proceso por cada una de las subqueries.
     * En caso de que no sea posible entonces tratamos la query como una unica
     *
     * retorna String o false en caso de que se haya podido dividir o no, el String es la transformacion
     */
    private function divideIntoBlocks($sheet, $cell, $query)
    {

        $ret = "";

        $isMulti = false;

        $pLevel     = 0;
        $pDQuote    = 0;
        $pSQuote    = 0;
        $isFunction = 0;

        $subQuery = "";

        for ($i = 0; $i < strlen($query); $i++) {
            $char = $query[$i];

            if ($pSQuote) { // estamos en ', solamente tenemos que cerrar o concatenar
                $subQuery .= $char;

                if ($char === "'") {
                    $pSQuote = false;
                }

                continue;
            }

            if ($pDQuote) { // estamos en ", solamente tenemos que cerrar o concatenar
                $subQuery .= $char;

                if ($char === '"') {
                    $pDQuote = false;
                }

                continue;
            }

            if ($char === "'") { // Entramos aqui siempre que no sea que estamos en un string
                $subQuery .= $char;
                $pSQuote  = true;
                continue;
            }

            if ($char === '"') { // Entramos aqui siempre que no sea que estamos en un string
                $subQuery .= $char;
                $pDQuote  = true;
                continue;
            }

            // Ahora tratamos los parentesis, similar a los strings

            if ($char === ")") { // cerramos parentesis
                $pLevel--;

                if (!$pLevel) {
                    $blocks[] = $subQuery;

                    // abrimos el parentesis en la respuesta, si y solo si no es parentesis de funcion
                    if ($isFunction) {
                        // miramos que no sea una funcion lo que estamos parseando
                        $subQuery   .= ")";
                        $isFunction = false;
                    }

                } else {
                    // guardamos el cierre si ya hemos abierto parentesis
                    $subQuery .= ")";
                }

                continue;
            }

            if ($char === "(") {
                if ($pLevel) {
                    // abrimos parentesis en la subquery
                    $subQuery .= "(";
                } else {
                    // abrimos el parentesis en la respuesta, si y solo si no es parentesis de funcion
                    if (strlen($subQuery)) {
                        // miramos que no sea una funcion lo que estamos parseando
                        $subQuery   .= "(";
                        $isFunction = 1;
                    }
                }

                $pLevel++;

                continue;
            }

            if ($pLevel) {
                // Si estamos en parentesis, solamente concatemos caracter y ya está.
                $subQuery .= $char;
                continue;
            }

            // si el caracter es un espacio sobrante, lo eliminamos
            if ($char == " ") {
                continue;
            }

            // Una subquery se divide en operaciones matematicas, asi que si no tenemos que estar en parentesis
            // en este punto
            if ($operation = $this->getOperation($query, $i)) {
                // si la operacion es compleja (2 o mas caracteres) avanzamos el iterador por caracteres de mas que tiene la operacion
                $i += strlen($operation[0]) - 1;

                // Como hemos encontrado una operacion, convertimos la subquery
                $isMulti = true;
                if ($subQuery == "") {
                    $ret .= "(0)" . " " . $operation[1] . " ";
                } else {
                    $ret .= $this->_parser($sheet, $cell, $subQuery) . " " . $operation[1] . " ";
                }
                $subQuery = "";
                continue;
            } else {
                // Concatenamos y a tomar por culo
                $subQuery .= $char;
                continue;
            }

        }

        // Hemos de comprobar que la query ha sido totalmente parseada, eso se sabe si el ultimo caracter de la query es un )
        if ($subQuery != "") {
            if ($isMulti) {
                $ret .= $this->_parser($sheet, $cell, $subQuery);

                return $ret;
            }
        } else if ($isMulti) {
            return $this->_parser($sheet, $cell, $subQuery);
        }

        return false;
    }

    /**
     * LEVEL2 estamos tratando con un bloque de parentesis
     */
    private function isAParenthesisGroup($query)
    {
        return ($query[0] === "(") && ($this->getLastChar($query) === ")");
    }

    /**
     * LEVEL3 estamos tratando con una funcion
     */
    private function _parseMethod($query)
    {

        if ($query[0] === "(") {
            return false;
        }

        $matches = [];

        /*$b = preg_match(self::HAS_PARENTHESIS, $query, $matches);

        if (!$b) {
            return false;
        }*/

        $b = preg_match(self::INV_REG_METHOD, $query, $matches);

        if ($b === 0) {
            return false;
        }

        if ($query == $matches[0]) {
            return false;
        }

        if ($matches[0] == $query) {
            return false;
        }

        $ret = [];

        $ret[] = $matches[0];
        $ret[] = substr(substr($query, strlen($matches[0])), 1, -1);

        if (!$ret[1]) {
            return false;
        }

        return $ret;
    }

    /**
     * LVL4 estamos parseando un acceso externo
     */
    private function _parseVar1($query)
    {
        $matches = [];

        $b = preg_match(self::REG_OUTER_RANGE, $query, $matches);

        if (!$b) {
            $b = preg_match(self::REG_OUTER_VAR, $query, $matches);

            if (!$b) {
                return false;
            }
        }

        /*
        preg_match(self::REG_OUTER_VAR, $query, $matches);

        if (count($matches)) {

        } else {
            return false;
        }
        //echo "\n\n$query\n";
        //var_dump($matches);
        */

        return $matches;
    }

    /**
     * LEVEL5 estamos parseando un acceso interno
     */
    private function _parseVar2($query)
    {
        $matches = [];

        $b = preg_match(self::REG_INNER_RANGE, $query, $matches);

        if (!$b) {
            $b = preg_match(self::REG_INNER_VAR, $query, $matches);

            if (!$b) {
                return false;
            }
        }

        return $matches;
    }

    /**
     * Utils
     */

    public function incrCol($col)
    {

        for ($i = 0; $i < self::$COLS; $i++) {
            if (self::$COLS[$i] == $col) {
                return self::$COLS[$i + 1];
            }
        }
    }

    /**
     * retorna un array con cada una de las queries, no las parsea, cada query estará dividida en , o ;
     */
    private function _parseParams($sheet, $cell, $query)
    {
        $ret = "";

        $isMulti = false;

        $pLevel     = 0;
        $pDQuote    = 0;
        $pSQuote    = 0;
        $isFunction = 0;

        $subQuery = "";

        $params = [];

        for ($i = 0; $i < strlen($query); $i++) {
            $char = $query[$i];

            if ($pSQuote) { // estamos en ', solamente tenemos que cerrar o concatenar
                $subQuery .= $char;

                if ($char === "'") {
                    $pSQuote = false;
                }

                continue;
            }

            if ($pDQuote) { // estamos en ", solamente tenemos que cerrar o concatenar
                $subQuery .= $char;

                if ($char === '"') {
                    $pDQuote = false;
                }

                continue;
            }

            if ($char === "'") { // Entramos aqui siempre que no sea que estamos en un string
                $subQuery .= $char;
                $pSQuote  = true;
                continue;
            }

            if ($char === '"') { // Entramos aqui siempre que no sea que estamos en un string
                $subQuery .= $char;
                $pDQuote  = true;
                continue;
            }

            // Ahora tratamos los parentesis, similar a los strings

            if ($char === ")") { // cerramos parentesis
                $pLevel--;

                if (!$pLevel) {
                    $blocks[] = $subQuery;

                    // abrimos el parentesis en la respuesta, si y solo si no es parentesis de funcion
                    if ($isFunction) {
                        // miramos que no sea una funcion lo que estamos parseando
                        $subQuery   .= ")";
                        $isFunction = false;
                    }

                } else {
                    // guardamos el cierre si ya hemos abierto parentesis
                    $subQuery .= ")";
                }

                continue;
            }

            if ($char === "(") {
                if ($pLevel) {
                    // abrimos parentesis en la subquery
                    $subQuery .= "(";
                } else {
                    // abrimos el parentesis en la respuesta, si y solo si no es parentesis de funcion
                    if (strlen($subQuery)) {
                        // miramos que no sea una funcion lo que estamos parseando
                        $subQuery   .= "(";
                        $isFunction = 1;
                    }
                }

                $pLevel++;

                continue;
            }

            if ($pLevel) {
                // Si estamos en parentesis, solamente concatemos caracter y ya está.
                $subQuery .= $char;
                continue;
            }

            // si el caracter es un espacio sobrante, lo eliminamos
            if ($char == " ") {
                continue;
            }

            if ($this->isDelimiter($query, $i)) {
                $params[] = $subQuery;
                $subQuery = "";
                continue;
            }

            $subQuery .= $char;
        }

        if (strlen($subQuery)) {
            $params[] = $subQuery;
        }

        return $params;
    }

    private function isDelimiter($query, $i)
    {
        return ($query[$i] === ",") || ($query[$i] === ";");
    }

    private function getLastChar($text)
    {
        return $text[strlen($text) - 1];
    }

    private function getOperation($query, $i)
    {
        $char = $query[$i];

        if ($char === '+' || $char === '-' || $char === '*' || $char === '/') {
            return [$char, $char];
        }

        if ($char === "^") {
            return [$char, "**"];
        }

        if ($char === "=") {
            return [$char, "=="];
        }

        if ($char === ">" && ($query[$i + 1] === "=")) {
            return [">=", ">="];
        }

        if ($char === "<" && ($query[$i + 1] === "=")) {
            return ["<=", "<="];
        }

        if ($char === "<" && ($query[$i + 1] === ">")) {
            return ["<>", "!="];
        }

        if ($char === "<") {
            return [$char, "<"];
        }

        if ($char === ">") {
            return [$char, ">"];
        }

        return false;
    }

    /**
     * Extras
     */
    private function cleanQuery($query)
    {
        $unwanted_array = ['Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y'];

        return strtr($query, $unwanted_array);
    }

    protected function printVar($var, $value = "")
    {
        if (!$this->debug) {
            return;
        }
        $bt    = debug_backtrace(false, 5);
        $debug = $bt[1]["class"] . "::" . $bt[1]["function"] . " @ " . $bt[0]["line"] . "\n";

        $debug = "\033[1;31m$debug\033[0m";

        $array = false;
        $dicc  = false;

        try {
            $array = is_array($value);
            $dicc  = get_class($value) === "stdClass";
        } catch (\Exception $e) {
        }

        if ($array || $dicc) {
            if ($dicc) {
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;34mOBJECT\033[0m", 100 + 34) . $debug;
            } else {
                $count = count($array);
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;34mARRAY ($count)\033[0m", 100 + 34) . $debug;
            }
            $this->printJson2($var, $value, 1);
        } else {
            if ($value === false) {
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;33mfalse\033[0m", 100 + 34) . $debug;
            } else if ($value === NULL) {
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;33mNULL\033[0m", 100 + 34) . $debug;
            } else if ($value === "") {
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;33m''\033[0m", 100 + 34) . $debug;
            } else if ($value === 0) {
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;33m0\033[0m", 100 + 34) . $debug;
            } else {
                echo $this->fill("\033[1;37m$var:\033[0m \033[1;33m$value\033[0m", 100 + 34) . $debug;
            }
        }
    }

    private function fill($query, $size)
    {
        $length = $size - strlen($query);
        for ($i = 0; $i < $length; $i++) {
            $query .= " ";
        }

        return $query;
    }

    private function printJson2($var, $json, $lvl = 0)
    {
        foreach ($json as $key => $value) {
            $array = false;
            $dicc  = false;

            try {
                $array = is_array($value);
                $dicc  = get_class($value) === "stdClass";
            } catch (\Exception $e) {
            }

            if (!($array || $dicc)) {
                $this->printSpaces($lvl);
                if ($value === false) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mfalse\033[0m\n";
                } else if ($value === NULL) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mNULL\033[0m\n";
                } else if ($value === "") {
                    echo "\033[1;37m$key:\033[0m \033[1;33m''\033[0m\n";
                } else if ($value === 0) {
                    echo "\033[1;37m$key:\033[0m \033[1;33m0\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;33m$value\033[0m\n";
                }
            } else {
                $this->printSpaces($lvl);
                $count = count($value);
                if ($dicc) {
                    echo "\033[1;37m$key:\033[0m \033[1;34mOBJECT\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;34mARRAY ($count)\033[0m\n";
                }
                $this->printJsonLvl($key, $lvl + 1, $value);
            }
        }
    }

    private function printJsonLvl($var, $lvl, $json)
    {
        foreach ($json as $key => $value) {
            $array = false;
            $dicc  = false;

            try {
                $array = is_array($value);
                $dicc  = get_class($value) === "stdClass";
            } catch (\Exception $e) {
            }

            if ($array || $dicc) {
                $this->printSpaces($lvl);
                $count = count($value);
                if ($dicc) {
                    echo "\033[1;37m$key:\033[0m \033[1;34mOBJECT\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;34mARRAY ($count)\033[0m\n";
                }
                $this->printJsonLvl($key, $lvl + 1, $value);
            } else {
                $this->printSpaces($lvl);
                if (is_string($value)) {
                    if (strlen($value) <= 100) {
                        echo "\033[1;37m$key:\033[0m \033[1;33m$value\033[0m\n";
                    } else {
                        echo "\033[1;37m$key:\033[0m \033[1;33m" . substr($value, 0, 100) . "...\033[0m\n";
                    }
                } else if ($value === false) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mfalse\033[0m\n";
                } else if ($value === NULL) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mNULL\033[0m\n";
                } else if ($value === "") {
                    echo "\033[1;37m$key:\033[0m \033[1;33m''\033[0m\n";
                } else if ($value === 0) {
                    echo "\033[1;37m$key:\033[0m \033[1;33m0\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;33m$value\033[0m\n";
                }
            }
        }
    }

    private function printSpaces($lvl)
    {
        $mult = 2;

        $total = $lvl * $mult;

        for ($i = 0; $i < $total; $i++) {
            echo " ";
        }
    }

    private function printLine()
    {
        if (!$this->debug) {
            return;
        }

        $bt    = debug_backtrace(false, 5);
        $debug = $bt[1]["class"] . "::" . $bt[1]["function"] . " @ " . $bt[0]["line"];

        $debug = "\033[1;30m$debug\033[0m";
        echo "\033[1;30m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m$debug\n";
    }
}
