<?php
declare(strict_types=1);
namespace Nakima\Excel;

use Nakima\Utils\String\Text;

/**
 * @author xgonzalez@nakima.es
 */

class PHP {

    protected $header;
    protected $body;
    protected $bodyEnd;
    protected $vars;
    protected $methods;
    protected $footer;
    protected $cells;

    protected $namespace;
    protected $class;
    protected $sheets;

    public function __construct($namespace, $class) {

        $this->namespace = $namespace;
        $this->class = $class;
        $this->sheets = [];
        $this->cells = [];

        $this->header = "";
        $this->header .= "<?php\n\n";
        $this->header .= "declare(strict_types=1);\n";
        $this->header .= "namespace $namespace;\n\n";
        $this->header .= "/**\n";
        $this->header .= " * Generated automatically thanks to xgonzalez@nakima.es\n";
        $this->header .= " */\n\n";
        $this->header .= "use Nakima\\Utils\\String\\Text;\n";
        $this->header .= "use Nakima\\Excel\\Functions;\n";
        $this->header .= "use Nakima\\Excel\\BaseExcel;\n";

        $this->body = "Class $class extends BaseExcel\n{\n";
        $this->bodyEnd = "}\n";

        $this->vars = "";
        $this->methods = [];
        $this->customMethods = [];
        $this->footer = "}\n";

        $this->vars .= "    public \$sheets;\n";
        $this->vars .= "    public \$cells;\n";
        $this->vars .= "    public \$parent;\n";

        $method = "";
        $method .= "    public function __construct(\$parent = null)\n    {\n";
        $method .= "        \$this->parent = \$parent;\n";
        $method .= "        \$this->cells = [];\n";
        $method .= "    }\n\n";
        $this->customMethods["__construct"] = $method;

        $method = "";
        $method .= "    public function __get(\$field)\n    {\n";
        $method .= "        return \$this->{\$field};\n";
        $method .= "    }\n\n";
        $this->customMethods["__get"] = $method;

        $method = "";
        $method .= "    public function __set(\$field, \$value)\n    {\n";
        $method .= "        \$this->{\$field} = \$value;\n";
        $method .= "        return \$this;\n";
        $method .= "    }\n\n";
        $this->customMethods["__set"] = $method;

        $method = "";
        $method .= "    public function _call(\$field)\n    {\n";
        $method .= "        return \$this->{\$field}();\n";
        $method .= "    }\n\n";
        $this->customMethods["_call"] = $method;

        $method = "";
        $method .= "    public function _callGet(\$field)\n    {\n";
        $method .= "        return \$this->{'get' . ucfirst(\$field)}();\n";
        $method .= "    }\n\n";
        $this->customMethods["_callGet"] = $method;

    }

    public function getName() {
        return $this->class;
    }

    public function getSheet($name) {
        $sheet = ucfirst(Text::slugify($name));

        $namespace = $this->namespace;
        $class = $this->class;
        if (!isset($this->sheets[$sheet])) {
            $this->header .= "use $namespace\\$sheet$class;\n";

            $this->sheets[$sheet] = new PHP($namespace, "$sheet$class");
        }

        return $this->sheets[$sheet];
    }

    public function getClass() {
        return $this->class;
    }

    public function generate($src) {

        $result = "";
        $result .= $this->header;
        $result .= "\n";
        $result .= $this->body;
        $result .= $this->vars;
        $result .= "\n";

        $first = true;

        $list = "";
        foreach ($this->sheets as $key => $value) {
            if (!$first) {
                $list .= ", ";
            }
            $first = false;
            $name = $value->getClass();
            $list .= "'$name' => new $name(\$this)";
            $value->generate($src);
        }

        $method =
<<<EOD

    public function loadSheet(string \$s): ?BaseExcel
    {
        if (\$this->parent) {
            return \$this->parent->loadSheet(\$s);
        }
        if (!\$this->sheets) {
            \$this->sheets = [$list];
        }
        return \$this->sheets[\$s];
    }

EOD;
        $this->customMethods["ac_sheets"] = $method;

        /*
         * ADD METHOD TO LOAD EVERY SHEET
         */

        foreach ($this->sheets as $key => $value) {
            $name = $value->getClass();
            $method =
<<<EOD

    public function load$name(): $name
    {
        return \$this->loadSheet('$name');
    }

EOD;
            $this->customMethods["ac_sheets_$name"] = $method;
        }


        $first = true;
        $method = "\n";
        $method .= "    public function getCells()\n    {\n";
        $method .= "        if (!\$this->cells) {\n";
        $method .= "            \$this->cells = [";
        foreach ($this->cells as $key => $value) {
            if (!$first) {
                $method .= ", ";
            }
            $first = false;
            $method .= "'$key'";
        }
        $method .= "];\n";
        $method .= "        }\n";
        $method .= "        return \$this->cells;\n";
        $method .= "    }\n\n";
        $this->customMethods["ac_cells"] = $method;

        $methods = array_merge($this->methods, $this->customMethods);
        foreach ($methods as $value) {
            $result .= $value;
        }
        $result .= $this->footer;

        $class = $this->class;
        file_put_contents("$src/$class.php", $result);
    }

    public function addMethod($method, $cell) {
        $cell = strtoupper($cell);

        if (!(isset($this->methods[$cell])) || (isset($this->customMethods[$cell]))) {
            $this->methods[$cell] = $method;
            $this->cells[$cell] = 1;
        }
    }


    public function addInput($cell, $name) {

        unset($this->methods[$cell]);

        if (isset($this->customMethods[$cell])) {
            throw new \Exception("Method about $name ($cell) cannot be overrided.");
        }

        $cell = strtoupper($cell);
        $var = Text::slugify($name);
        $varUc = ucfirst($var);

        $this->vars .= "    public \$$var;\n";

        $this->cells[$cell] = 1;

        $this->customMethods[$cell] =
<<<EOD

    public function get$cell()
    {
        return \$this->$var;
    }

EOD;

        $method = "";
        $method .= "    public function get$varUc()\n    {\n";
        $method .= "        return \$this->$var;\n";
        $method .= "    }\n\n";
        $this->customMethods["aa_$cell"] = $method;

        $method = "";
        $method .= "    public function set$varUc(\$$var)\n    {\n";
        $method .= "        \$this->$var = \$$var;\n";
        $method .= "        return \$this;\n";
        $method .= "    }\n\n";
        $this->customMethods["ab_$cell"] = $method;
    }

    public function addOutput($cell, $name) {
        if (isset($this->customMethods[$cell])) {
            throw new \Exception("Method about $name ($cell) cannot be overrided.");
        }

        $cell = strtoupper($cell);
        $var = Text::slugify($name);
        $varUc = ucfirst($var);

        $method = "";
        $method .= "    public function get$varUc()\n    {\n";
        $method .= "        return \$this->get$cell();\n";
        $method .= "    }\n\n";
        $this->customMethods["ac_$cell"] = $method;
    }
}
