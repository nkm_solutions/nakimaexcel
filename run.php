<?php

require __DIR__ . '/config/autoload.php';
require __DIR__ . '/vendor/autoload.php';

error_reporting(E_ERROR | E_PARSE);
error_reporting(E_ALL); ini_set('display_errors', 1);

(new \Nakima\Excel\Main())->run();